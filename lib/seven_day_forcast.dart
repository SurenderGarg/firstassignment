import 'package:first_assignment/datamodel/WeatherData.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import 'hourly_weather_item.dart';
import 'seven_day_weather_item.dart';

Widget sevenDayForcast(List<Daily>? daily, bool isDarkMode){
  return Container(

    child: Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
           Text(
            "7-day forecast",
            style: TextStyle(
                fontSize: 20, fontWeight: FontWeight.w600, color: isDarkMode ? Colors.white : Colors.black),
          ),
          Divider( color: isDarkMode ?Colors.grey : Colors.black,),
          ListView.builder(
            physics: const NeverScrollableScrollPhysics(),
            shrinkWrap: true,
              scrollDirection: Axis.vertical,
              itemCount: daily?.length,
              itemBuilder: (context, index) {
                print('Data... ${daily?.elementAt(index).dt}');
                print('Data... ${DateFormat('EE').format(DateTime.fromMillisecondsSinceEpoch(daily?.elementAt(index)?.dt??0))}');
                return sevenDayWeatherItem(daily?.elementAt(index),isDarkMode);
              }),
        ],
      ),
    ),
  );
}