import 'package:first_assignment/datamodel/CategoryData.dart';
import 'package:first_assignment/datamodel/StoryData.dart';
import 'package:first_assignment/widgets/CategoryWidget.dart';
import 'package:first_assignment/widgets/CityWidget.dart';
import 'package:first_assignment/widgets/HotelWidget.dart';
import 'package:first_assignment/widgets/PeopleWidget.dart';
import 'package:first_assignment/widgets/PopularWidget.dart';
import 'package:first_assignment/widgets/SearchWidget.dart';
import 'package:first_assignment/widgets/StoryWidget.dart';
import 'package:first_assignment/widgets/TextFieldWidget.dart';
import 'package:flutter/material.dart';

import 'datamodel/CitiesData.dart';
import 'datamodel/PeopleData.dart';

class ThirdAssignment extends StatefulWidget {
  String title;

    ThirdAssignment({required this.title});

  @override
  State<StatefulWidget> createState() => _ThirdAssignmentState();
}

class _ThirdAssignmentState extends State<ThirdAssignment> {
  final _serachController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "Explore",
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 20,
                          fontWeight: FontWeight.w700),
                    ),
                    Stack(
                      children: [
                        Stack(
                          children: [
                            Icon(
                              Icons.notification_add_outlined,
                              size: 25,
                            ),
                            Positioned(
                              top: 5,
                              right: 5,
                              height: 10,
                              width: 10,
                              child: CircleAvatar(
                                backgroundImage: AssetImage(
                                    'assets/images/loginbackgroud.jpg'),
                                backgroundColor: Colors.blue,
                              ),
                            ),
                          ],
                        )
                      ],
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 10),
                child: Row(
                  children: [
                    Expanded(
                        child: SizedBox(
                      height: 80,
                      child: ListView.separated(
                          scrollDirection: Axis.horizontal,
                          itemCount: CategoryList.length,
                          separatorBuilder: (BuildContext context, int index) =>
                              const SizedBox(
                                width: 30,
                              ),
                          itemBuilder: (context, index) {
                            final people = CategoryList[index];
                            return CategoryWidget(categoryData: people);
                          }),
                    )),
                  ],
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 10),
                child: SearchWidget(
                  icon: Icons.search,
                  hintText: "Search City",
                  readOnly: false,
                  isPassword: false,
                  textEditingController: _serachController,
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 10),
                child: Row(
                  children: [
                    Container(
                      height: 180,
                      width: 100,
                      decoration: BoxDecoration(
                          border: Border.all(width: 2, color: Colors.black12),
                          borderRadius: BorderRadius.circular(15.0)),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Expanded(
                        child: SizedBox(
                      height: 180,
                      child: ListView.separated(
                          scrollDirection: Axis.horizontal,
                          itemCount: StoryList.length,
                          separatorBuilder: (BuildContext context, int index) =>
                              const SizedBox(
                                width: 10,
                              ),
                          itemBuilder: (context, index) {
                            final people = StoryList[index];
                            return StoryWidget(categoryData: people);
                          }),
                    )),
                  ],
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 10),
                child: Text(
                  "Most Popular Now",
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 25,
                      fontWeight: FontWeight.w700),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 10),
                child: Container(
                  height: 100,
                  child: Expanded(
                      child: ListView.separated(
                          scrollDirection: Axis.horizontal,
                          itemCount: StoryList.length,
                          separatorBuilder: (BuildContext context, int index) =>
                              const SizedBox(
                                width: 10,
                              ),
                          itemBuilder: (context, index) {
                            final people = StoryList[index];
                            return PopularWidget(categoryData: people);
                          })),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 10),
                child: Container(
                  height: 100,
                  child: Expanded(
                      child: ListView.separated(
                          scrollDirection: Axis.horizontal,
                          itemCount: StoryList.length,
                          separatorBuilder: (BuildContext context, int index) =>
                              const SizedBox(
                                width: 10,
                              ),
                          itemBuilder: (context, index) {
                            final people = StoryList[index];
                            return PopularWidget(categoryData: people);
                          })),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 10),
                child: Container(
                  height: 100,
                  child: Expanded(
                      child: ListView.separated(
                          scrollDirection: Axis.horizontal,
                          itemCount: StoryList.length,
                          separatorBuilder: (BuildContext context, int index) =>
                              const SizedBox(
                                width: 10,
                              ),
                          itemBuilder: (context, index) {
                            final people = StoryList[index];
                            return PopularWidget(categoryData: people);
                          })),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Stack(
                children: [
                  Container(
                    height: 500,
                    decoration: BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage('assets/images/background.jpeg'),
                            fit: BoxFit.cover)),
                  ),
                  Container(
                    height: 500,
                    width: double.infinity,
                    decoration: BoxDecoration(
                        gradient: LinearGradient(
                            begin: Alignment.topCenter,
                            end: Alignment.bottomCenter,
                            colors: [
                          Colors.black.withOpacity(0.0),
                          Colors.black.withOpacity(0.5)
                        ])),
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Text(
                            "Poland",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 20,
                                fontWeight: FontWeight.w500),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Text(
                            "A Spellbinding cities where culture collide",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 15,
                                fontWeight: FontWeight.w300),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Container(
                            height: 130,
                            child: Expanded(
                                child: ListView.separated(
                                    scrollDirection: Axis.horizontal,
                                    itemCount: CitiesList.length,
                                    separatorBuilder:
                                        (BuildContext context, int index) =>
                                            const SizedBox(
                                              width: 10,
                                            ),
                                    itemBuilder: (context, index) {
                                      final people = CitiesList[index];
                                      return CityWidget(categoryData: people);
                                    })),
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
              SizedBox(
                height: 20,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "Hotels",
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 20,
                        fontWeight: FontWeight.w500),
                  ),
                  Text(
                    "See All",
                    style: TextStyle(
                        color: Colors.blue,
                        fontSize: 15,
                        fontWeight: FontWeight.w500),
                  ),
                ],
              ),
              SizedBox(
                height: 20,
              ),
              Container(
                height: 300,
                width: double.infinity,
                child: Expanded(
                    child: ListView.separated(
                        scrollDirection: Axis.horizontal,
                        itemCount: StoryList.length,
                        separatorBuilder: (BuildContext context, int index) =>
                            const SizedBox(
                              width: 10,
                            ),
                        itemBuilder: (context, index) {
                          final people = StoryList[index];
                          return HotelWidget(categoryData: people);
                        })),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
