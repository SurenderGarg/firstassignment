import 'package:first_assignment/widgets/LoginBtn.dart';
import 'package:first_assignment/RegistrationScreen.dart';
import 'package:first_assignment/widgets/SocialBtn.dart';
import 'package:first_assignment/widgets/TextFieldWidget.dart';
import 'package:flutter/material.dart';

import 'LoginEmailScreen.dart';

class Socailogin extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _SocailoginState();
}

class _SocailoginState extends State<Socailogin> {

  _navigateLogin() {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => LoginScreen()));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Container(
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage('assets/images/loginbackground.jpg'),
                    fit: BoxFit.cover)),
          ),
          Container(
            height: double.infinity,
            width: double.infinity,
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [
                  Colors.black.withOpacity(0.3),
                  Colors.black.withOpacity(0.9)
                ])),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Text(
                  "Welcome Social",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 20,
                      fontWeight: FontWeight.bold),
                ),
                Text(
                  "Join Mr. BookWarm!",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 15,
                      fontWeight: FontWeight.bold),
                ),
                SocailBtn(
                  title: "Google",
                  titleColor: Colors.black,
                  bgColor: Colors.blueAccent,
                  borderColor: Colors.blue,
                  isIconVisible: true,
                  icon: 'assets/images/loginbackground.jpg',
                ),
                SocailBtn(
                  title: "Facebook",
                  titleColor: Colors.black,
                  bgColor: Colors.white,
                  borderColor: Colors.white,
                  isIconVisible: true,
                  icon: 'assets/images/loginbackground.jpg',
                ),
                LoginBtn(onTap: _navigateLogin, title: "Login"),

                Center(
                  child: Text(
                    "Don't have an account",
                    style: TextStyle(color: Colors.white, fontSize: 10),
                  ),
                ),
                Center(
                    child: TextButton(
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => RegistrationScreen()));
                  },
                  child: Text(
                    "Create Account",
                    style: TextStyle(color: Colors.white),
                  ),
                )),
                SizedBox(
                  height: 50,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
