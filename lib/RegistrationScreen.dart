import 'package:first_assignment/widgets/LoginBtn.dart';
import 'package:first_assignment/widgets/TextFieldWidget.dart';
import 'package:flutter/material.dart';

import 'LoginEmailScreen.dart';

class RegistrationScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _RegistrationScreenState();
}

class _RegistrationScreenState extends State<RegistrationScreen> {
  final _emailCoontriller = TextEditingController();
  final _cityContriller = TextEditingController();
  final _mobileContriller = TextEditingController();
  final _collegeContriller = TextEditingController();
  final _passwordCoontriller = TextEditingController();

  _validateField() {
    if (_emailCoontriller.text.length > 0) {
      _showErrorMsg("Please enter Email Id");
    } else if (_passwordCoontriller.text.length > 6) {
      _showErrorMsg("Please enter valid password");
    }else{
      _showErrorMsg("Please enter Email password");
    }
  }

  _showErrorMsg(String msg) {
    ScaffoldMessenger.of(context).clearSnackBars();
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(msg)));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Container(
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage('assets/images/loginbackground.jpg'),
                    fit: BoxFit.cover)),
          ),
          Container(
            height: double.infinity,
            width: double.infinity,
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [
                      Colors.black.withOpacity(0.3),
                      Colors.black.withOpacity(0.9)
                    ])),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Text(
                  "Welcome",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 20,
                      fontWeight: FontWeight.bold),
                ),
                Text(
                  "Join Mr. BookWarm!",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 15,
                      fontWeight: FontWeight.bold),
                ),
                TextFiledWidget(
                  icon: Icons.email,
                  hintText: "Enter Email",
                  readOnly: false,
                  isPassword: false,
                  textEditingController: _emailCoontriller,
                ),
                TextFiledWidget(
                  icon: Icons.location_city_outlined,
                  hintText: "City",
                  readOnly: false,
                  isPassword: false,
                  textEditingController: _cityContriller,
                ),
                TextFiledWidget(
                  icon: Icons.dialpad,
                  hintText: "Mobile Number",
                  readOnly: false,
                  isPassword: false,
                  textEditingController: _mobileContriller,
                ),
                TextFiledWidget(
                  icon: Icons.school,
                  hintText: "College",
                  readOnly: false,
                  isPassword: false,
                  textEditingController: _passwordCoontriller,
                ),
                TextFiledWidget(
                  icon: Icons.password,
                  hintText: "Password",
                  readOnly: false,
                  isPassword: true,
                  textEditingController: _passwordCoontriller,
                ),
                LoginBtn(onTap: _validateField, title: "Register"),
                Center(
                  child: Text("Already have an account",
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 10
                  ),),
                ),
                Center(
                  child:TextButton(
                   onPressed: (){
                     Navigator.push(context, MaterialPageRoute(builder: (context)=>LoginScreen()));
                   },
                    child: Text("Login",
                    style: TextStyle(
                      color:Colors.white
                    ),),

                  )
                ),
                SizedBox(
                  height: 50,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
