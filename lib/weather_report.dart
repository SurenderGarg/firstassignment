import 'dart:convert';

import 'package:first_assignment/current_weather.dart';
import 'package:first_assignment/datamodel/WeatherData.dart';
import 'package:first_assignment/hourly_weather.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';

import 'datamodel/current_weather_data.dart';

//28.4089° N, 77.3178° E Faridabad

// current   https://api.openweathermap.org/data/2.5/weather?lat=28.4089&lon=77.3178&appid=cc95d932d5a45d33a9527d5019475f2c&units=metric
// hourly    https://api.openweathermap.org/data/2.5/onecall?lat=28.7041&lon=77.1025&cnt=5&exclude=current,minutely,daily&appid=cc95d932d5a45d33a9527d5019475f2c&units=metric
// daily    https://api.openweathermap.org/data/2.5/onecall?lat=28.7041&lon=77.1025&exclude=current,minutely,hourly,alerts&appid=cc95d932d5a45d33a9527d5019475f2c&units=metric

// current : https://api.openweathermap.org/data/2.5/forecast?lat=28
// .7041&lon=77.1025&cnt=3&appid=cc95d932d5a45d33a9527d5019475f2c
// hourly : https://api.openweathermap.org/data/2.5/onecall?lat=28.7041&lon=77
// .1025&exclude=current,minutely,daily&appid=cc95d932d5a45d33a9527d5019475f2c&units=metric
// daily: https://api.openweathermap.org/data/2.5/onecall?lat=28.7041&lon=77
// .1025&exclude=current,minutely,hourly,alerts&appid=cc95d932d5a45d33a9527d5019475f2c&units=metric

class weather_report extends StatefulWidget {
  weather_report({Key? key, required this.title}) : super(key: key);
  String title;

  @override
  State<weather_report> createState() => _weather_reportState();
}

class _weather_reportState extends State<weather_report> {
  late Future<CurrentWeatherData> todayWeatherModel;
  late Future<WeatherData> weatherData;
  static const String todayDataUrl =
      'https://api.openweathermap.org/data/2.5/weather?lat=28.4089&lon=77.3178&appid=cc95d932d5a45d33a9527d5019475f2c&units=metric';
  static const String weatherDataUrl =
      'https://api.openweathermap.org/data/2.5/onecall?lat=28.7041&lon=77.1025&exclude=current,minutely,alerts&appid=cc95d932d5a45d33a9527d5019475f2c&units=metric';

  Future<CurrentWeatherData> getTodayWeather() async {
    final response = await http.get(Uri.parse(todayDataUrl));

    if (response.statusCode == 200) {
      var millis = 1687852800;
      // var dt = DateTime.fromMillisecondsSinceEpoch(millis);
      // var dt1000 = DateTime.fromMillisecondsSinceEpoch(millis*1000);
      // print('date: $dt');
      // print('date: $dt1000');
// 12 Hour format:
//       var d12 = DateFormat('hh a').format(dt);

      // print('date: $d12');
      // print('date: ${DateFormat('EEEE').format(dt)}');
      // print('date: ${DateFormat('EE').format(dt)}');
      // print('date: ${DateFormat('EEE').format(dt)}');

// 12/31/2000, 10:00 PM
// 24 Hour format:
// var d24 = DateFormat('dd/MM/yyyy, HH:mm').format(dt);
// 31/12/2000, 22:00
      // If the server did return a 200 OK response,
      // then parse the JSON.
      return CurrentWeatherData.fromJson(jsonDecode(response.body));
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to load album');
    }
  }

  Future<WeatherData> getWeatherData() async {
    final response = await http.get(Uri.parse(weatherDataUrl));

    if (response.statusCode == 200) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      return WeatherData.fromJson(jsonDecode(response.body));
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to load album');
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    todayWeatherModel = getTodayWeather();
    weatherData = getWeatherData();
  }

  @override
  Widget build(BuildContext context) {
    var brightness = MediaQuery.of(context).platformBrightness;
    bool isDarkMode = brightness == Brightness.dark;
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title,style: TextStyle(
          color: isDarkMode ?Colors.white :Colors.black,
          fontSize: 15.0
        ),),
        backgroundColor: isDarkMode?Colors.black :Colors.white,
        centerTitle: true,
        leading: IconButton(
          onPressed: () {},
          icon: Icon(Icons.menu),
        ),
      ),
      body: Container(
        width: double.infinity,
        decoration: BoxDecoration(
          color: isDarkMode ? Colors.black : Colors.white,
        ),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              FutureBuilder<CurrentWeatherData>(
                future: todayWeatherModel,
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    return currentWeather(Icons.cloud_outlined, snapshot.data,isDarkMode);
                  } else if (snapshot.hasError) {
                    return Text('${snapshot.error}');
                  }

                  // By default, show a loading spinner.
                  return const Text("");
                },
              ),
              const SizedBox(
                height: 10,
              ),
              FutureBuilder<WeatherData>(
                future: weatherData,
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    return hourlyWeather(snapshot.data,
                        MediaQuery.of(context).size.height / 3,isDarkMode);
                  } else if (snapshot.hasError) {
                    print('${snapshot.stackTrace}');
                    return Text('${snapshot.error}');
                  }

                  // By default, show a loading spinner.
                  return const CircularProgressIndicator();
                },
              ),
              // const Divider(
              //   color: Colors.black,
              //   thickness: 5.0,
              // ),
            ],
          ),
        ),
      ),
      // body: GoogleMap(),
    );
  }
}
