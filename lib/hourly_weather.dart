import 'package:first_assignment/datamodel/WeatherData.dart';
import 'package:first_assignment/hourly_weather_item.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import 'seven_day_forcast.dart';

Widget hourlyWeather(WeatherData? weatherData,double height, bool isDarkMode) {
  return Padding(
    padding: const EdgeInsets.all(8.0),
    child: Column(
      children: [
        Container(
          height: height,
          width: double.infinity,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10.0),
              color:isDarkMode ? Colors.black : Colors.grey[300]
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: EdgeInsets.all(8.0),
                child: Text(
                  "ForeCast for Today",
                  style: TextStyle(fontWeight: FontWeight.w500, fontSize: 20,
                      color: isDarkMode ? Colors.white : Colors.black),
                ),
              ),
              Expanded(
                child: SizedBox(
                  child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      itemCount: weatherData?.hourly?.length,
                      itemBuilder: (context, index) {
                        print('Data...hour  ${DateFormat('hh a').format(DateTime.fromMillisecondsSinceEpoch(weatherData?.hourly?.elementAt(index)?.dt??0))}');
                        return hourlyWeatherItem(weatherData?.hourly?.elementAt(index),isDarkMode);
                      }),
                ),
              ),
            ],
          ),
        ),
        sevenDayForcast(weatherData?.daily,isDarkMode),
      ],
    ),
  );
}
