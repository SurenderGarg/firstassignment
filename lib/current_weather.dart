import 'package:flutter/material.dart';

import 'datamodel/current_weather_data.dart';

Widget currentWeather(IconData icon, CurrentWeatherData? todayWeather, bool isDarkMode) {

  return Column(
    mainAxisAlignment: MainAxisAlignment.center,
    crossAxisAlignment: CrossAxisAlignment.center,
    children: [
      const SizedBox(
        height: 10,
      ),
      Text(
        todayWeather?.name.toString() ?? "Faridabad",
        style: TextStyle(
            fontSize: 46,
            fontWeight: FontWeight.w600,
            color: isDarkMode ? Colors.white : Colors.black
        ),
      ),
      Text(
        "Today",
        style: TextStyle(
            fontSize: 25, fontWeight: FontWeight.w400,
            color: isDarkMode ? Colors.grey : Colors.black38),
      ),
      const SizedBox(
        height: 10,
      ),
      Text(
        "${todayWeather?.main?.temp.toString()} ˚C" ?? "0.0",
        style: const TextStyle(
            fontSize: 50,
            fontWeight: FontWeight.w400,
            color: Colors.blueAccent),
      ),
      const Padding(
        padding: EdgeInsets.symmetric(vertical: 0.0, horizontal: 80.0),
        child: Divider(
          color: Colors.grey,
        ),
      ),
      const SizedBox(
        height: 10,
      ),
      Text(
        todayWeather?.weather?.first?.main ?? "",
        style: TextStyle(
            fontSize: 25, fontWeight: FontWeight.w400,
            color: isDarkMode ? Colors.grey : Colors.black38),
      ),
      const SizedBox(
        height: 10,
      ),
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            "${todayWeather?.main?.tempMin?.toString()} ˚C" ??"",
            style: const TextStyle(
                fontSize: 20, fontWeight: FontWeight.w400, color: Colors.blue),
          ),
          Text(
            "/",
            style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.w400,
                color: isDarkMode ? Colors.grey : Colors.black38),
          ),
          Text(
            "${todayWeather?.main?.tempMax?.toString()} ˚C" ??"",
            style: const TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.w400,
                color: Colors.blueAccent),
          ),
        ],
      )
    ],
  );
}
