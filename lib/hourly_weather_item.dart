import 'package:first_assignment/datamodel/WeatherData.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

Widget hourlyWeatherItem(Hourly? hourElement, bool isDarkMode) {
  return Padding(
    padding: const EdgeInsets.all(8.0),
    child: Column(
      children: [
        Text(
          DateFormat('hh a').format(DateTime.fromMillisecondsSinceEpoch(hourElement?.dt??0)),
          style:  TextStyle(
              fontSize: 15, fontWeight: FontWeight.w400,
              color: isDarkMode ? Colors.white : Colors.black),
        ),
        const SizedBox(height: 5.0,),
        Icon(
          Icons.cloud,
          color: isDarkMode ?Colors.white :Colors.black,
          size: 30,
        ),
        const SizedBox(height: 5.0,),
        Text(
          "${hourElement?.temp?.toString()}˚C" ??"",
          style:  TextStyle(
              fontSize: 16, fontWeight: FontWeight.w400,
              color: isDarkMode ? Colors.white : Colors.black),
        ),
        Icon(
          Icons.closed_caption_off_rounded,
       color:  isDarkMode ? Colors.white:Colors.black,
          size: 30,
        ),
        const SizedBox(height: 5.0,),
        Text(
          '${hourElement?.windSpeed?.toString()} km/h',
          style: const TextStyle(
              fontSize: 15, fontWeight: FontWeight.w400, color: Colors.grey),
        ),
        const SizedBox(height: 5.0,),
        const Icon(
          Icons.umbrella_sharp,
          size: 30,
          color: Colors.blue,
        ),
        const SizedBox(height: 5.0,),
        Text(
          "${hourElement?.pop?.toString()}%",
          style: const TextStyle(
              fontSize: 15, fontWeight: FontWeight.w400, color: Colors.blue),
        )
      ],
    ),
  );
}
