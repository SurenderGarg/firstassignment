import 'package:first_assignment/LoginEmailScreen.dart';
import 'package:first_assignment/weather_report.dart';
import 'package:first_assignment/widgets/SecondAssignment.dart';
import 'package:flutter/material.dart';

import 'Socialogin.dart';
import 'ThirdAssignment.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: "Assiment"),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
      title: Text(widget.title),
    ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            InkWell(
              focusColor: Colors.blueAccent,
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => Socailogin()));
              },
              child: Container(
                padding: EdgeInsets.all(20),
                color: Colors.blueAccent,
                child: const Text(
                  'First Assignment',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 25,
                  ),
                ),
              ),
            ),
            SizedBox(height: 10),
            InkWell(
              focusColor: Colors.blueAccent,
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => SecondAssignment(title: "Second")));
              },
              child: Container(
                padding: EdgeInsets.all(20),
                color: Colors.blueAccent,
                child: const Text(
                  'Second Assignment',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 25,
                  ),
                ),
              ),
            ),
            SizedBox(height: 10),
            InkWell(
              focusColor: Colors.blueAccent,
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => ThirdAssignment(title: "ThirdAssignment")));
              },
              child: Container(
                padding: EdgeInsets.all(20),
                color: Colors.blueAccent,
                child: const Text(
                  'Third Assignment',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 25,
                  ),
                ),
              ),
            ),
            SizedBox(height: 10),
            InkWell(
              focusColor: Colors.blueAccent,
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => weather_report(title: "Weather Report")));
              },
              child: Container(
                padding: EdgeInsets.all(20),
                color: Colors.blueAccent,
                child: const Text(
                  'Weather Report',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 25,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: const Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
