import 'package:first_assignment/datamodel/GridItem.dart';
import 'package:flutter/material.dart';

class GriditemWidget extends StatelessWidget {
  final GridItem gridItem;

  GriditemWidget({required this.gridItem});

  @override
  Widget build(BuildContext context) {
    return Card(
      color: gridItem.cardBackgroundColor,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 14, vertical: 18),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            gridItem.icon,
            SizedBox(height: 12),
            Text(
              gridItem.title,
              style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
            ),
            SizedBox(height: 12),
            Text(
              gridItem.subTitle,
              style: TextStyle(
                  fontSize: 15,
                  fontWeight: FontWeight.bold,
                  color: Colors.black12),
            ),
          ],
        ),
      ),
    );
  }
}
