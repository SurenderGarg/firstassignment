import 'package:flutter/material.dart';

class LoginBtn extends StatefulWidget {
  Function() onTap;
  String title;

  LoginBtn({required this.onTap, required this.title});

  @override
  State<StatefulWidget> createState() => _LoginBtn();
}

class _LoginBtn extends State<LoginBtn> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(15),
      child: InkWell(
        onTap: widget.onTap,
        child: Container(
          height: 50,
          width: double.infinity,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(25), color: Colors.white),
          child: Center(
            child: Text(
              widget.title,
              style: TextStyle(fontWeight: FontWeight.normal, fontSize: 16),
            ),
          ),
        ),
      ),
    );
  }
}
