import 'package:first_assignment/datamodel/PeopleData.dart';
import 'package:flutter/material.dart';

import '../datamodel/CategoryData.dart';

class CategoryWidget extends StatelessWidget {
  CategoryWidget({required this.categoryData});

  CategoryData categoryData;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          child: (categoryData.categoryUrl != null)
              ? CircleAvatar(
                  radius: 25,
                  backgroundImage: AssetImage(categoryData.categoryUrl!),
                  backgroundColor: Colors.grey,
                )
              : CircleAvatar(
                  backgroundColor: Colors.blueGrey,
                ),
        ),
        SizedBox(
          height: 10,
        ),
        Text(
          categoryData.categoryName,
          style: TextStyle(color: Colors.grey, fontWeight: FontWeight.w600),
        ),
      ],
    );
  }
}
