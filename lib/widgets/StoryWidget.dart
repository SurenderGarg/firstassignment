import 'package:first_assignment/datamodel/PeopleData.dart';
import 'package:flutter/material.dart';

import '../datamodel/CategoryData.dart';
import '../datamodel/StoryData.dart';

class StoryWidget extends StatelessWidget {
  StoryWidget({required this.categoryData});

  StoryData categoryData;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          height: 180,
          width: 100,
          child: Stack(
            children: [
              ClipRRect(
                  borderRadius: BorderRadius.circular(10),
                  child: Image(
                    image: AssetImage(categoryData.storyUrl),
                    height: 180,
                    width: 100,
                    fit: BoxFit.fill,
                  ),
              ),
              Positioned(
                left: 5,
                top: 5,
                child: CircleAvatar(
                  radius: 20,
                  backgroundColor: Colors.white,
                  child: CircleAvatar(
                    radius: 19,
                    backgroundImage: AssetImage(categoryData.storyIcon!),
                  ),
                ),
              )
            ],
          ),
        ),
      ],
    );
  }
}
