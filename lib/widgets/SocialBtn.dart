import 'package:flutter/material.dart';

class SocailBtn extends StatefulWidget {
  String title;
  Color titleColor;
  Color bgColor;
  Color borderColor;
  bool isIconVisible;
  String icon;
  Function()? onTap;

  SocailBtn(
      {required this.title,
      required this.titleColor,
      required this.bgColor,
      required this.borderColor,
      required this.isIconVisible,
      required this.icon,
      this.onTap});

  @override
  State<StatefulWidget> createState() => _SocailBtnState();
}

class _SocailBtnState extends State<SocailBtn> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(15),
      child: InkWell(
        onTap: () {
          if (widget.onTap != null) {
          } else
            "${widget.title}";
        },
        child: Container(
          height: 50,
          width: double.infinity,
          padding: EdgeInsets.only(left: 15, right: 50),
          decoration: BoxDecoration(
              border: Border.all(color: widget.borderColor, width: 2),
              borderRadius: BorderRadius.circular(25),
              color: widget.bgColor),
          child: Row(
            children: [
              Visibility(
                visible: widget.isIconVisible,
                child: Container(
                  height: 40,
                  width: 40,
                  color: Colors.transparent,
                  child: Image.asset(widget.icon),
                ),
              ),
              // SizedBox(
              //   width: 20,
              // ),
              Expanded(
                child: Text(
                  widget.title,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontWeight: FontWeight.normal,
                      fontSize: 16,
                      color: widget.titleColor),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
