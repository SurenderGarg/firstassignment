import 'package:first_assignment/datamodel/CitiesData.dart';
import 'package:first_assignment/datamodel/PeopleData.dart';
import 'package:flutter/material.dart';

import '../datamodel/CategoryData.dart';
import '../datamodel/StoryData.dart';

class CityWidget extends StatelessWidget {
  CityWidget({required this.categoryData});

  CitiesData categoryData;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        ClipRRect(
          borderRadius: BorderRadius.circular(10),
          child: Image(
            image: AssetImage(categoryData.cityUrl),
            height: 100,
            width: 150,
            fit: BoxFit.fill,
          ),
        ),
        SizedBox(
          height: 10,
        ),
        Text(
          categoryData.cityName,
          style: TextStyle(
              color: Colors.white,
              fontSize: 15,
              fontWeight: FontWeight.w400),
        ),
      ],
    );
  }
}
