import 'package:first_assignment/datamodel/PeopleData.dart';
import 'package:flutter/material.dart';

class PeopleWidget extends StatelessWidget{
  PeopleWidget({required this.peopledata});

  Peopledata peopledata;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(2),
      child: SizedBox(
        height: 60,
        width: 60,
        child: Stack(
          clipBehavior: Clip.none,
          fit: StackFit.expand,
          children: [
            (peopledata.profileUrl != null) ? CircleAvatar(
              backgroundImage: AssetImage(peopledata.profileUrl!),
              backgroundColor: Colors.grey,
            )
                : CircleAvatar(
              backgroundColor: Colors.blueGrey,
              child: Text(peopledata.userName.substring(0, 2).toUpperCase()),
            ),
            Positioned(
              right: -5,
              bottom: 5,
              height: 25,
              width: 25,
              child: CircleAvatar(
                backgroundImage: AssetImage(peopledata.countryIconUrl),
                backgroundColor: Colors.grey,
              ),
            ),
          ],
        ),
      ),
    );
  }

}