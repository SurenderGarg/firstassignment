import 'package:flutter/material.dart';

class TextFiledWidget extends StatefulWidget {
  bool readOnly;
  bool isPassword;
  Function()? onTap;
  String hintText;
  TextEditingController? textEditingController;
  IconData icon;

  TextFiledWidget(
      {required this.hintText,required this.readOnly, required this.isPassword, this.onTap = null,this.textEditingController,required this.icon});

  @override
  State<StatefulWidget> createState() => _TextFieldWidget();
}

class _TextFieldWidget extends State<TextFiledWidget> {
  var boxDeco = BoxDecoration(
      border: Border.all(width: 2, color: Colors.white),
      borderRadius: BorderRadius.circular(25.0));

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Padding(
      padding: const EdgeInsets.all(10),
      child: Container(
        padding: EdgeInsets.only(left: 15, right: 50),
        height: 50,
        decoration: boxDeco,
        child: Row(
          children: [
            Icon(
              widget.icon,
              color: Colors.brown,
            ),
            SizedBox(
              width: 20,
            ),
            Expanded(
                child: TextField(
              readOnly: widget.readOnly,
              onTap: widget.onTap,
              controller: widget.textEditingController,
                  style: TextStyle(
                    color: Colors.white,
                  ),
                  textAlign: TextAlign.center,
                  obscureText: widget.isPassword,
                  decoration: InputDecoration(
                    hintText: widget.hintText,
                    hintStyle: TextStyle(
                      color: Colors.white
                    ),
                    border: InputBorder.none
                  ),
            ))
          ],
        ),
      ),
    );
  }
}
