import 'package:flutter/material.dart';

class SearchWidget extends StatefulWidget {
  bool readOnly;
  bool isPassword;
  Function()? onTap;
  String hintText;
  TextEditingController? textEditingController;
  IconData icon;

  SearchWidget(
      {required this.hintText,required this.readOnly, required this.isPassword, this.onTap = null,this.textEditingController,required this.icon});

  @override
  State<StatefulWidget> createState() => _SearchWidget();
}

class _SearchWidget extends State<SearchWidget> {
  var boxDeco = BoxDecoration(
      border: Border.all(width: 2, color: Colors.red),
      borderRadius: BorderRadius.circular(15.0));

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      padding: EdgeInsets.only(left: 15, right: 50),
      height: 40,
      decoration: boxDeco,
      child: Row(
        children: [
          Icon(
            widget.icon,
            color: Colors.brown,
          ),
          SizedBox(
            width: 20,
          ),
          Expanded(
              child: TextField(
            readOnly: widget.readOnly,
            onTap: widget.onTap,
            controller: widget.textEditingController,
                style: TextStyle(
                  color: Colors.grey,
                ),
                textAlign: TextAlign.left,
                obscureText: widget.isPassword,
                decoration: InputDecoration(
                  hintText: widget.hintText,
                  hintStyle: TextStyle(
                    color: Colors.grey
                  ),
                  border: InputBorder.none
                ),
          ))
        ],
      ),
    );
  }
}
