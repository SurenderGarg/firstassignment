import 'package:first_assignment/widgets/PeopleWidget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

import '../datamodel/GridItem.dart';
import '../datamodel/PeopleData.dart';
import 'GridItemWidget.dart';

class SecondAssignment extends StatefulWidget {
  String title;

  SecondAssignment({
    required this.title,
  });

  @override
  State<StatefulWidget> createState() => _SecondAssignmentState();
}

class _SecondAssignmentState extends State<SecondAssignment> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: 5, horizontal: 15),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "Hi Mugambo,",
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 15,
                        fontWeight: FontWeight.w400),
                  ),
                  Stack(
                    children: [
                      Stack(
                        children: [
                          Icon(Icons.notifications),
                          Positioned(
                            top: 2,
                            right: 2,
                            height: 10,
                            width: 10,
                            child: CircleAvatar(
                                backgroundImage: AssetImage(
                                    'assets/images/loginbackgroud.jpg'),
                            ),

                          ),
                        ],
                      )
                    ],
                  ),
                ],
              ),
              Text(
                "1,234.00",
                textAlign: TextAlign.left,
                textDirection: TextDirection.ltr,
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 25,
                    fontWeight: FontWeight.bold),
              ),
              SizedBox(
                height: 5,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Center(
                    child: CircleAvatar(
                      radius: 10,
                      backgroundImage: AssetImage('assets/images/second .jpeg'),
                    ),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Text(
                    "GHS",
                    style: TextStyle(fontSize: 10, fontWeight: FontWeight.w500),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Icon(Icons.keyboard_arrow_down)
                ],
              ),
              SizedBox(
                height: 10,
              ),
              Text(
                "Here are some things you can do",
                style: TextStyle(
                    fontSize: 12,
                    fontWeight: FontWeight.w400,
                    color: Colors.black),
              ),
              SizedBox(
                height: 30,
              ),
              Expanded(
                child: GridView.builder(
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 2, crossAxisSpacing: 5),
                    itemCount: gridItemList.length,
                    itemBuilder: (BuildContext context, int index) {
                      final gridItem = gridItemList[index];
                      return GriditemWidget(gridItem: gridItem);
                    }),
              ),
              Text(
                "Your favourite people",
                style: TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.w400,
                    color: Colors.black),
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    height: 60,
                    width: 60,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: Colors.grey,
                    ),
                    child: Icon(
                      Icons.add,
                      color: Colors.black38,
                      size: 35,
                    ),
                  ),
                  SizedBox(
                    width: 8,
                  ),
                  Expanded(
                      child: SizedBox(
                    height: 90,
                    child: ListView.builder(
                        scrollDirection: Axis.horizontal,
                        itemCount: PeopleList.length,
                        itemBuilder: (context, index) {
                          final people = PeopleList[index];
                          return PeopleWidget(peopledata: people);
                        }),
                  ))
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
