import 'package:first_assignment/datamodel/PeopleData.dart';
import 'package:flutter/material.dart';

import '../datamodel/CategoryData.dart';
import '../datamodel/StoryData.dart';

class PopularWidget extends StatelessWidget {
  PopularWidget({required this.categoryData});

  StoryData categoryData;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        ClipRRect(
          borderRadius: BorderRadius.circular(10),
          child: Image(
            image: AssetImage(categoryData.storyUrl),
            height: 100,
            width: 150,
            fit: BoxFit.fill,
          ),
        ),
        SizedBox(
          width: 30,
        ),
        Text(
          "1",
          style: TextStyle(
              fontSize: 20, fontWeight: FontWeight.w400, color: Colors.grey),
        ),
        SizedBox(
          width: 20,
        ),
        Container(
          width: 150,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "Place of Culture and Science",
                style: TextStyle(
                    fontSize: 15, fontWeight: FontWeight.w400, color: Colors.black),
              ),
              Text(
                "WarSaw",
                style: TextStyle(
                    fontSize: 10, fontWeight: FontWeight.w300, color: Colors.grey),
              ),


            ],
          ),
        )
      ],
    );
  }
}
