import 'package:flutter/material.dart';

class Peopledata {
  Peopledata(
      {required this.userName,
        required this.profileUrl,
        required this.countryIconUrl});

  final String userName;
  final String? profileUrl;
  final String countryIconUrl;
}

final PeopleList = [
  Peopledata(
      userName: 'Gaurav',
      profileUrl: 'assets/images/second .jpeg',
      countryIconUrl: 'assets/images/loginbackgroud.jpg'),
  Peopledata(
      userName: 'Albert',
      profileUrl: 'assets/images/second .jpeg',
      countryIconUrl: 'assets/images/second .jpeg'),
];