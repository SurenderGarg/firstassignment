class CitiesData {
  CitiesData({
    required this.cityUrl,
    required this.cityName,
  });

  final String cityUrl;
  final String cityName;
}

final CitiesList = [
  CitiesData(cityName: 'Warsaw', cityUrl: 'assets/images/second .jpeg'),
  CitiesData(cityName: 'Warsaw', cityUrl: 'assets/images/second .jpeg'),
  CitiesData(cityName: 'Warsaw', cityUrl: 'assets/images/second .jpeg'),
  CitiesData(cityName: 'Warsaw', cityUrl: 'assets/images/second .jpeg'),
  CitiesData(cityName: 'Warsaw', cityUrl: 'assets/images/second .jpeg'),
  CitiesData(cityName: 'Warsaw', cityUrl: 'assets/images/second .jpeg'),
  CitiesData(cityName: 'Warsaw', cityUrl: 'assets/images/second .jpeg'),
];
