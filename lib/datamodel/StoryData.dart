class StoryData{
  StoryData(
      {required this.storyUrl,
        required this.storyIcon,
      });

  final String storyUrl;
  final String? storyIcon;
}

final StoryList = [
  StoryData(
      storyUrl: 'assets/images/second .jpeg',
      storyIcon: 'assets/images/second .jpeg'),
  StoryData(
      storyUrl: 'assets/images/second .jpeg',
      storyIcon: 'assets/images/second .jpeg'),
  StoryData(
      storyUrl: 'assets/images/second .jpeg',
      storyIcon: 'assets/images/second .jpeg'),
  StoryData(
      storyUrl: 'assets/images/second .jpeg',
      storyIcon: 'assets/images/second .jpeg'),
  StoryData(
      storyUrl: 'assets/images/second .jpeg',
      storyIcon: 'assets/images/second .jpeg'),

];