import 'package:first_assignment/datamodel/WeatherData.dart';
import 'package:first_assignment/hourly_weather_item.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

Widget sevenDayWeatherItem(Daily? dailyItem, bool isDarkMode) {
  return Padding(
    padding: const EdgeInsets.all(8.0),
    child: Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [

            Text(
              DateFormat('EE').format(DateTime.fromMillisecondsSinceEpoch(dailyItem?.dt??0)),
              style:  TextStyle(
                  fontSize: 15, fontWeight: FontWeight.w400, color: isDarkMode?Colors.white :Colors.black),
            ),
            Icon(
              Icons.cloud,
              color:  isDarkMode ? Colors.white:Colors.black,
              size: 30,
            ),
            Text(
              "${dailyItem?.temp?.min}˚C",
              style: const TextStyle(
                  fontSize: 16, fontWeight: FontWeight.w400, color: Colors.grey),
            ),
            Text(
              "${dailyItem?.temp?.max}˚C",
              style:  TextStyle(
                  fontSize: 16, fontWeight: FontWeight.w400, color: isDarkMode ? Colors.white :Colors.black),
            ),
          ],
        ),
        Divider()
      ],
    ),

  );
}
